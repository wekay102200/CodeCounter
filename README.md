#CodeCounter

CodeCounter --help information

	CodeCounter path
	useage:
	   path: code file path or code dir path
	  -ignore-comment: ignore code comment or not [DEFAULT True] 
	  -ignore-blank-line: ignore code blank line or not [DEFAULT True] 
	  -exclude-dir: add exclude code dir
	  -exclude-file: add exclude code file
